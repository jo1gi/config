{ config, pkgs, ... }:

let
  partisiaMail = "joakim.holm@secata.com";
  gitlabToken = builtins.readFile ./secrets/gitlab_private_token;
  partisiaPassword = builtins.readFile ./secrets/password;
in
{
  imports = [
    ../../home.nix
    ./kanshi.nix
  ];

  config = {

    jo1gi = {
      terminal.fontsize = 13;
      general.enable = true;
      programming = {
        java = {
          enable = true;
          jdk = pkgs.jdk17;
          maven = {
            enable = true;
            options = [
              "-Dmaven.gitcommitid.skip=true"
            ];
          };
        };
        nim.enable = true;
        nodejs.enable = true;
        rust = {
          enable = true;
          useRustup = true;
        };
      };
    };

    programs = {
      glab.enable = true;
      findProject = {
        dir = "$HOME/Projects";
      };
    };

    # xsession.windowManager.i3.enable = true;
    wayland.windowManager.sway.enable = true;

    home.packages = with pkgs; [
      jetbrains.idea-community
      # jetbrains.idea-ultimate
      watchman

      kubectl
      minikube
      pulumi-bin

      create-docker-image
      spotless
    ];

    home.sessionPath = [
      "$HOME/bin"
    ];

    home.sessionVariables = {
      GITLAB_PRIVATE_TOKEN = gitlabToken;
      PULUMI_CONFIG_PASSPHRASE = "";
    };

    home.file = {
      "${config.home.homeDirectory}/.netrc" = {
        text = ''
          machine nexus.secata.com login ${partisiaMail} password ${partisiaPassword}
        '';
      };
      "${config.home.homeDirectory}/.m2/settings.xml" = {
        text = (import ./maven_config.nix){
          partisia_mail = partisiaMail;
          partisia_password = partisiaPassword;
          partisia_gitlab_token = gitlabToken;
        };
      };
    };

    nixpkgs.overlays = [
      (self: super: {
        create-docker-image = pkgs.callPackage ./scripts/create-docker-image.nix {};
        spotless = pkgs.callPackage ./scripts/spotless.nix {};
      })
    ];

  };

}
