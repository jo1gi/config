{ config, ... }:

{
  config.services.kanshi = {
    enable = true;
    settings = [
      {
        profile = {
          name = "default";
          outputs = [
            { criteria = "DP-4"; }
            { criteria = "eDP-1"; status = "disable"; }
            { criteria = "DP-2"; status = "disable"; }
          ];
        };
      }
    ];
  };
}
