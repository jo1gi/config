#!/usr/bin/env raku
unit sub MAIN(Bool :$push = False);

if "pom.xml".IO.e {
    run "mvn", "spotless:apply";
}
if "cargo.toml".IO.e {
    run "cargo", "fmt", "--all";
}

if $push {
    run "git", "add", ".";
    run "git", "commit", "-m", "Spotless";
    run "git", "push";
}
