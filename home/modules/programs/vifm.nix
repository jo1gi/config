{ config, lib, pkgs, ... }:

{
  config = lib.mkIf config.programs.vifm.enable {
    programs.neovim = {
      plugins = with pkgs.vimPlugins; [
        vifm-vim
      ];
    };
  };
}
