{ config, lib, pkgs, ... }:

let
  cfg = config.jo1gi.programming.freemarker;
in
{
  config = lib.mkIf cfg.enable {
    programs.neovim = {
      extraLuaConfig = ''
        vim.filetype.add({
          extensions = {
            flth = "freemarker.html"
          }
        })
      '';
      plugins = [
        (pkgs.vimUtils.buildVimPlugin {
          pname = "vim-freemarker";
          version = "1";
          src = pkgs.fetchFromGitHub {
            owner = "andreshazard";
            repo = "vim-freemarker";
            rev = "993bda23e72e4c074659970c1e777cb19d8cf93e";
            sha256 = "sha256-g4GnutHqxOH0rhZZmx7YpqFWZ9a+lTC6SdNYvVrSPbY=";
          };
        })
      ];
    };
  };

  options.jo1gi.programming.freemarker = {
    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };
  };
}
