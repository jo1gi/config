{ config, lib, pkgs, ... }:

{
  config = lib.mkIf config.jo1gi.programming.gleam.enable {
    home.packages = with pkgs; [
      gleam
      erlang_27
    ];

    programs.neovim = {
      extraLuaConfig = ''
        require('jo1gi.helpers.setup_lsp')("gleam")
      '';
    };
  };

  options.jo1gi.programming.gleam = {

    enable = lib.mkOption {
      type = lib.types.bool;
      default = false;
    };

  };
}
